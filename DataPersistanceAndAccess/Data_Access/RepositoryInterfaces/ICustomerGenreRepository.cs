﻿using DataPersistanceAndAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Data_Access.Interfaces
{
    public interface ICustomerGenreRepository
    {
        /// <summary>
        /// Gets the most popular genre(s) by an customer ID
        /// </summary>
        /// <param name="customerID">The Customer ID from Customer</param>
        /// <returns>A list containing either one or several genres related to the favorite genre(s)</returns>
        List<CustomerGenre> GetMostPopularGenreByCustomerID(int customerID);
    }
}
