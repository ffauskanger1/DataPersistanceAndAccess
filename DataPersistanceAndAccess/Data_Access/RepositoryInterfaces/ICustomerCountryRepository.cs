﻿using DataPersistanceAndAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Data_Access.Interfaces
{
    public interface ICustomerCountryRepository
    {
        /// <summary>
        /// Gets the customers country listed by high to low
        /// </summary>
        /// <returns>A sorted (high to low) List of CostumerCountry</returns>
        public List<CustomerCountry> GetCustomersPerCountry();

    }
}
