﻿using DataPersistanceAndAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Data_Access.Interfaces
{
    public interface ICustomerRepository
    {
        /// <summary>
        /// Runs a SQL query and reads all the results into customers.
        /// </summary>
        /// <returns>A list of all the customers</returns>
        List<Customer> GetAllCustomers();
        /// <summary>
        /// Runs a SQL query and reads all the results into customers by a limit and offset in the tables
        /// </summary>
        /// <param name="limit">Limit rows</param>
        /// <param name="offset">Offset rows</param>
        /// <returns>A list of customers that is offset and limit</returns>
        List<Customer> GetAllCustomersByLimitAndOffset(int limit, int offset);
        /// <summary>
        /// Runs a SQL query and reads an customer by supplying the ID
        /// </summary>
        /// <param name="id">The Customer ID</param>
        /// <returns>A Customer object</returns>
        Customer GetCustomerByID(int id);
        /// <summary>
        /// Runs a SQL query and reads an customer by supplying the name
        /// </summary>
        /// <param name="lastName">The lastname to search by</param>
        /// <returns>An customer object that matches</returns>
        Customer GetCustomerByName(string lastName);
        /// <summary>
        /// Inserts a customer into the DB by a SQL query
        /// </summary>
        /// <param name="customer">The customer object to insert</param>
        /// <returns>If any rows was affected, returns true, else false</returns>
        bool InsertCustomer(Customer customer);
        /// <summary>
        /// Updates a customer into the DB by a SQL query
        /// </summary>
        /// <param name="customer">The customer object to update</param>
        /// <returns>If any rows was affected, returns true, else false</returns>
        bool UpdateCustomer(Customer customer);
    }
}
