﻿using DataPersistanceAndAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Data_Access.Interfaces
{
    public interface ICustomerSpenderRepository
    {
        /// <summary>
        /// Runs an SQL query to get the highest spenders based on their invoice fees.
        /// </summary>
        /// <returns>A list of CustomerSpender objects, sorted by highest to lowest spenders</returns>
        List<CustomerSpender> GetHighestSpenders();
    }
}
