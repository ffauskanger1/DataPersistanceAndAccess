﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Data_Access.DbHelpers
{
    /// <summary>
    /// Helper class to setup connection to DB
    /// </summary>
    public class ChinookDbConfig
    {
        /// <summary>
        /// Builds the connection string for the Chinook DB
        /// </summary>
        /// <returns>The Connection string to Chinook DB</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "localhost\\SQLEXPRESS";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;

            return builder.ConnectionString;
        }
    }
}
