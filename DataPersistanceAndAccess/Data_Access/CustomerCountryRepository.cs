﻿using DataPersistanceAndAccess.Data_Access.DbHelpers;
using DataPersistanceAndAccess.Data_Access.Interfaces;
using DataPersistanceAndAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Data_Access
{
    ///<inheritdoc[cref = "ICustomerCountryRepository"]/>
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        public List<CustomerCountry> GetCustomersPerCountry()
        {
            List<CustomerCountry> customersPerCountry = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(*) AS Amount " +
                "FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY Amount DESC";

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);

                using SqlDataReader reader = command.ExecuteReader();

                // Gets the ordinals (numbers) related to the table
                int countryOrdinal = reader.GetOrdinal("Country");
                int amountOrdinal = reader.GetOrdinal("Amount");

                while (reader.Read())
                {
                    customersPerCountry.Add(new CustomerCountry()
                    {
                        Name = reader.IsDBNull(countryOrdinal) ? null : reader.GetString(countryOrdinal),
                        Count = reader.GetInt32(amountOrdinal),
                    });
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customersPerCountry;
        }
    }
}
