﻿using DataPersistanceAndAccess.Data_Access.DbHelpers;
using DataPersistanceAndAccess.Data_Access.Interfaces;
using DataPersistanceAndAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Data_Access
{
    ///<inheritdoc[cref = "ICustomerGenreRepository"]/>
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        public List<CustomerGenre> GetMostPopularGenreByCustomerID(int customerID)
        {
            List<CustomerGenre> popularGenresOfCustomer = new List<CustomerGenre>();

            string sql = "SELECT TOP 1 WITH TIES Genre.Name, COUNT(Genre.Name) as Total,Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                "FROM(Customer " +
                "INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId " +
                "INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId " +
                "INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId " +
                "INNER JOIN Genre ON Genre.GenreId = Track.GenreId) " +
                "WHERE Invoice.CustomerId = @customerID " +
                "GROUP BY Genre.Name, Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                "ORDER BY Total DESC";

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("@customerID", customerID);

                using SqlDataReader reader = command.ExecuteReader();

                // Gets the ordinals (numbers) related to the table
                int customerIdOrdinal = reader.GetOrdinal("CustomerId");
                int firstNameOrdinal = reader.GetOrdinal("FirstName");
                int lastNameOrdinal = reader.GetOrdinal("LastName");
                int GenreNameOrdinal = reader.GetOrdinal("Name");

                while (reader.Read())
                {
                    popularGenresOfCustomer.Add(new CustomerGenre() {
                        CustomerId = reader.GetInt32(customerIdOrdinal),
                        CustomerFirstName = reader.IsDBNull(firstNameOrdinal) ? null : reader.GetString(firstNameOrdinal),
                        CustomerLastName = reader.IsDBNull(lastNameOrdinal) ? null : reader.GetString(lastNameOrdinal),
                        GenreName = reader.IsDBNull(GenreNameOrdinal) ? null : reader.GetString(GenreNameOrdinal),
                    });
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return popularGenresOfCustomer;
        }
    }
}
