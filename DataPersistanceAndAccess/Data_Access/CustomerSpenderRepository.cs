﻿using DataPersistanceAndAccess.Data_Access.DbHelpers;
using DataPersistanceAndAccess.Data_Access.Interfaces;
using DataPersistanceAndAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Data_Access
{
    ///<inheritdoc[cref = "ICustomerSpenderRepository"]/>
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();
            string sql = "SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, SUM(Total) AS TotalSpent  " +
                "FROM (Customer " +
                "INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId) " +
                "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                "ORDER BY TotalSpent DESC";

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);

                using SqlDataReader reader = command.ExecuteReader();

                // Gets the ordinals (numbers) related to the table
                int customerIdOrdinal = reader.GetOrdinal("CustomerId");
                int totalOrdinal = reader.GetOrdinal("TotalSpent");
                int firstNameOrdinal = reader.GetOrdinal("FirstName");
                int lastNameOrdinal = reader.GetOrdinal("LastName");

                while (reader.Read())
                {
                    customerSpenders.Add(new CustomerSpender() { 
                        CustomerId = reader.GetInt32(customerIdOrdinal), 
                        Total = reader.GetDecimal(totalOrdinal),
                        CustomerFirstName = reader.IsDBNull(firstNameOrdinal) ? null : reader.GetString(firstNameOrdinal),
                        CustomerLastName = reader.IsDBNull(lastNameOrdinal) ? null : reader.GetString(lastNameOrdinal),
                    });
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerSpenders;
        }
    }
}
