﻿using DataPersistanceAndAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Resources;
using DataPersistanceAndAccess.Data_Access.DbHelpers;
using DataPersistanceAndAccess.Data_Access.Interfaces;

namespace DataPersistanceAndAccess.Data_Access
{
    ///<inheritdoc[cref = "ICustomerRepository"]/>
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();

            string sql = "SELECT * FROM Customer";

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);

                using SqlDataReader reader = command.ExecuteReader();

                // Gets the ordinals (numbers) related to the table
                int customerIdOrdinal = reader.GetOrdinal("CustomerId");
                int firstNameOrdinal = reader.GetOrdinal("FirstName");
                int lastNameOrdinal = reader.GetOrdinal("LastName");
                int countryOrdinal = reader.GetOrdinal("Country");
                int postalCodeOrdinal = reader.GetOrdinal("PostalCode");
                int phoneOrdinal = reader.GetOrdinal("Phone");
                int emailOrdinal = reader.GetOrdinal("Email");

                while (reader.Read())
                {
                    customers.Add(new Customer()
                    {
                        Id = reader.GetInt32(customerIdOrdinal),
                        FirstName = reader.IsDBNull(firstNameOrdinal) ? null : reader.GetString(firstNameOrdinal),
                        LastName = reader.IsDBNull(lastNameOrdinal) ? null : reader.GetString(lastNameOrdinal),
                        Country = reader.IsDBNull(countryOrdinal) ? null : reader.GetString(countryOrdinal), 
                        PostalCode = reader.IsDBNull(postalCodeOrdinal) ? null : reader.GetString(postalCodeOrdinal),
                        Phone = reader.IsDBNull(phoneOrdinal) ? null : reader.GetString(phoneOrdinal),
                        Email = reader.IsDBNull(emailOrdinal) ? null : reader.GetString(emailOrdinal),
                    });
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customers;
        }

        public List<Customer> GetAllCustomersByLimitAndOffset(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();

            string sql = "SELECT * FROM Customer " +
                "ORDER BY CustomerId " +
                "OFFSET (@offset) ROWS " +
                "FETCH NEXT (@limit) ROWS ONLY";

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("@limit", limit);
                command.Parameters.AddWithValue("@offset", offset);

                using SqlDataReader reader = command.ExecuteReader();

                // Gets the ordinals (numbers) related to the table
                int customerIdOrdinal = reader.GetOrdinal("CustomerId");
                int firstNameOrdinal = reader.GetOrdinal("FirstName");
                int lastNameOrdinal = reader.GetOrdinal("LastName");
                int countryOrdinal = reader.GetOrdinal("Country");
                int postalCodeOrdinal = reader.GetOrdinal("PostalCode");
                int phoneOrdinal = reader.GetOrdinal("Phone");
                int emailOrdinal = reader.GetOrdinal("Email");


                while (reader.Read())
                {
                    customers.Add(new Customer()
                    {
                        Id = reader.GetInt32(customerIdOrdinal),
                        FirstName = reader.IsDBNull(firstNameOrdinal) ? null : reader.GetString(firstNameOrdinal),
                        LastName = reader.IsDBNull(lastNameOrdinal) ? null : reader.GetString(lastNameOrdinal),
                        Country = reader.IsDBNull(countryOrdinal) ? null : reader.GetString(countryOrdinal),
                        PostalCode = reader.IsDBNull(postalCodeOrdinal) ? null : reader.GetString(postalCodeOrdinal),
                        Phone = reader.IsDBNull(phoneOrdinal) ? null : reader.GetString(phoneOrdinal),
                        Email = reader.IsDBNull(emailOrdinal) ? null : reader.GetString(emailOrdinal),
                    });
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customers;
        }

        public Customer GetCustomerByID(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT * FROM Customer " +
                "WHERE CustomerId = @customerID";

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("@customerID", id);

                using SqlDataReader reader = command.ExecuteReader();

                // Gets the ordinals (numbers) related to the table
                int customerIdOrdinal = reader.GetOrdinal("CustomerId");
                int firstNameOrdinal = reader.GetOrdinal("FirstName");
                int lastNameOrdinal = reader.GetOrdinal("LastName");
                int countryOrdinal = reader.GetOrdinal("Country");
                int postalCodeOrdinal = reader.GetOrdinal("PostalCode");
                int phoneOrdinal = reader.GetOrdinal("Phone");
                int emailOrdinal = reader.GetOrdinal("Email");

                reader.Read();
                customer = new Customer() //FIXME no constructor?
                {
                    Id = reader.GetInt32(customerIdOrdinal),
                    FirstName = reader.IsDBNull(firstNameOrdinal) ? null : reader.GetString(firstNameOrdinal),
                    LastName = reader.IsDBNull(lastNameOrdinal) ? null : reader.GetString(lastNameOrdinal),
                    Country = reader.IsDBNull(countryOrdinal) ? null : reader.GetString(countryOrdinal),
                    PostalCode = reader.IsDBNull(postalCodeOrdinal) ? null : reader.GetString(postalCodeOrdinal),
                    Phone = reader.IsDBNull(phoneOrdinal) ? null : reader.GetString(phoneOrdinal),
                    Email = reader.IsDBNull(emailOrdinal) ? null : reader.GetString(emailOrdinal),
                };

                
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public Customer GetCustomerByName(string lastName)
        {
            Customer customer = new Customer();
            string sql = "SELECT * FROM Customer " +
                "WHERE LastName LIKE @LastName";

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.AddWithValue("@LastName", "%" + lastName + "%"); // Adds wildcards to parameter

                using SqlDataReader reader = command.ExecuteReader();

                // Gets the ordinals (numbers) related to the table
                int customerIdOrdinal = reader.GetOrdinal("CustomerId");
                int firstNameOrdinal = reader.GetOrdinal("FirstName");
                int lastNameOrdinal = reader.GetOrdinal("LastName");
                int countryOrdinal = reader.GetOrdinal("Country");
                int postalCodeOrdinal = reader.GetOrdinal("PostalCode");
                int phoneOrdinal = reader.GetOrdinal("Phone");
                int emailOrdinal = reader.GetOrdinal("Email");

                reader.Read();
                customer = new Customer() 
                {
                    Id = reader.GetInt32(customerIdOrdinal),
                    FirstName = reader.IsDBNull(firstNameOrdinal) ? null : reader.GetString(firstNameOrdinal),
                    LastName = reader.IsDBNull(lastNameOrdinal) ? null : reader.GetString(lastNameOrdinal),
                    Country = reader.IsDBNull(countryOrdinal) ? null : reader.GetString(countryOrdinal),
                    PostalCode = reader.IsDBNull(postalCodeOrdinal) ? null : reader.GetString(postalCodeOrdinal),
                    Phone = reader.IsDBNull(phoneOrdinal) ? null : reader.GetString(phoneOrdinal),
                    Email = reader.IsDBNull(emailOrdinal) ? null : reader.GetString(emailOrdinal),
                };
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }
        public bool InsertCustomer(Customer customer)
        {
            int rowsAffected = 0;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.Phone);
                command.Parameters.AddWithValue("@Email", customer.Email);

                rowsAffected = command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return rowsAffected > 0;
        }

        public bool UpdateCustomer(Customer customer)
        {
            int rowsAffected = 0;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode =  @PostalCode, Phone = @Phone, Email = @Email " +
                "WHERE CustomerId = @customerID";

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();

                using SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("@customerID", customer.Id);
                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@Phone", customer.Phone);
                command.Parameters.AddWithValue("@Email", customer.Email);

                rowsAffected = command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return rowsAffected > 0;
        }
    }
}
