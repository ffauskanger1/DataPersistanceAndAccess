﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Models
{
    /// <summary>
    /// Model for CustomerCountry object
    /// </summary>
    public class CustomerCountry
    {

        public string Name { get; set; }
        public int Count { get; set; }
    }
}
