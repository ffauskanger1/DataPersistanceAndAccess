﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Models
{
    /// <summary>
    /// Model for CustomerGenre object
    /// </summary>
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string GenreName { get; set; }

    }
}
