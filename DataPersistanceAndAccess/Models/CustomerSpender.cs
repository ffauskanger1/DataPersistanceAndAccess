﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataPersistanceAndAccess.Models
{
    /// <summary>
    /// Model for CustomerSpender object
    /// </summary>
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public decimal Total { get; set; }
    }
}
