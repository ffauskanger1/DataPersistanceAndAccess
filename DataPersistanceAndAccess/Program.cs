﻿using DataPersistanceAndAccess.Data_Access;
using DataPersistanceAndAccess.Models;
using System;
using System.Collections.Generic;

namespace DataPersistanceAndAccess
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Uncomment to test the different methods with test variables
            //PrintAllCustomers();
            //PrintCustomerByID(1);
            //PrintCustomerByName("ilip");
            //PrintCustomersByLimitAndOffset(5, 10);
            //InsertCustomer();
            //UpdateCustomer();
            //PrintCustomersPerCountry();
            //PrintHighestSpenders();
            //PrintMostPopularGenreOfCustomer(1);
        }

        static void PrintAllCustomers()
        {
            CustomerRepository customerRepository = new CustomerRepository();
            List<Customer> customers = customerRepository.GetAllCustomers();

            foreach (var customer in customers)
            {
                Console.WriteLine($"{customer.Id}\t{customer.FirstName}\t{customer.LastName}\t{customer.Phone}\t{customer.Email}\t{customer.Phone}\t{customer.PostalCode}");
            }
        }

        static void PrintCustomerByID(int id)
        {
            CustomerRepository customerRepository = new CustomerRepository();
            Customer customer = customerRepository.GetCustomerByID(id);
            Console.WriteLine($"{customer.Id}\t{customer.FirstName}\t{customer.LastName}\t{customer.Phone}\t{customer.Email}\t{customer.Phone}\t{customer.PostalCode}");
        }
        
        static void PrintCustomersByLimitAndOffset(int limit, int offset)
        {
            CustomerRepository customerRepository = new CustomerRepository();
            List<Customer> customers = customerRepository.GetAllCustomersByLimitAndOffset(limit, offset);

            foreach (var customer in customers)
            {
                Console.WriteLine($"{customer.Id}\t{customer.FirstName}\t{customer.LastName}\t{customer.Phone}\t{customer.Email}");
            }
        }

        static void InsertCustomer()
        {
            CustomerRepository customerRepository = new CustomerRepository();
            Customer customer = new Customer { FirstName = "Warren", LastName = "West", Phone = "123456789", PostalCode = "1450", Email = "warrenwest@lol.com", Country = "South Africa"};

            customerRepository.InsertCustomer(customer);
        }

        static void PrintMostPopularGenreOfCustomer(int id)
        {
            CustomerGenreRepository customerGenreRepository = new CustomerGenreRepository();
            List<CustomerGenre> totalGenres = customerGenreRepository.GetMostPopularGenreByCustomerID(id);
            foreach (CustomerGenre customerGenre in totalGenres)
            {
                Console.WriteLine($"{customerGenre.CustomerId}\t{customerGenre.CustomerFirstName}\t{customerGenre.CustomerLastName}\t{customerGenre.GenreName}");
            }
        }

        static void PrintHighestSpenders()
        {
            CustomerSpenderRepository customerSpenderRepository = new CustomerSpenderRepository();
            List<CustomerSpender> customerSpenders = customerSpenderRepository.GetHighestSpenders();

            foreach (CustomerSpender customerSpender in customerSpenders)
            {
                Console.WriteLine($"{customerSpender.CustomerId}\t{customerSpender.CustomerFirstName}\t{customerSpender.CustomerLastName}\t{customerSpender.Total}");
            }
        }

        static void PrintCustomersPerCountry()
        {
            CustomerCountryRepository customerCountryRepository = new CustomerCountryRepository();
            List<CustomerCountry> customersPerCountry = customerCountryRepository.GetCustomersPerCountry();

            foreach (var customerCountry in customersPerCountry)
            {
                Console.WriteLine($"{customerCountry.Name}\t{customerCountry.Count}");
            }
        }

        static void PrintCustomerByName(string lastname) // FIXME
        {
            CustomerRepository customerRepository = new CustomerRepository();
            Customer customer = customerRepository.GetCustomerByName(lastname);
            Console.WriteLine($"{customer.Id}\t{customer.FirstName}\t{customer.LastName}\t{customer.Phone}\t{customer.Email}\t{customer.PostalCode}");
        }

        static void UpdateCustomer()
        {
            CustomerRepository customerRepository = new CustomerRepository();
            Customer customer = customerRepository.GetCustomerByID(1);
            customer.FirstName = "Mark";
            customerRepository.UpdateCustomer(customer);
        }
    }
}
