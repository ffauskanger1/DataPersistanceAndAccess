CREATE TABLE Superhero_Powers(
CONSTRAINT SuperheroPowerID PRIMARY KEY(SuperheroID, PowerID),
SuperheroID int FOREIGN KEY REFERENCES Superhero(Id),
PowerID int FOREIGN KEY REFERENCES Power(Id)
);