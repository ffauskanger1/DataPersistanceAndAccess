CREATE TABLE Superhero (
    ID int IDENTITY(1,1) PRIMARY KEY,
    Name nvarchar(100),
    Alias nvarchar(100),
	Origin nvarchar(100),
);

CREATE TABLE Assistant (
    ID int IDENTITY(1,1) PRIMARY KEY,
    Name nvarchar(100),
);

CREATE TABLE Power (
    ID int IDENTITY(1,1) PRIMARY KEY,
    Name nvarchar(100),
    Description nvarchar(100),
);