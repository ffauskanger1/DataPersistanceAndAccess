# Assignment 2: DataPersistanceAndAccess

## Description

This project is the assignment 2 in the Noroff .NET Accelerate program. It has been worked on using the agile technique "Pair programming". 

The project consists of SQL Queries for Appendix A.
Appendix B is part of the VS solution, where SQL queries has been performed on Models by the Data_Access folder.
There is some test methods in Program.cs that can be uncommented.

## Contributors
The contributors of this project is: Fredrik Fauskanger and Sindre Tornes Steinsvik